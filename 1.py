from web3 import Web3
import json

# Establishing connection
url='HTTP://127.0.0.1:7545'
web3=Web3(Web3.HTTPProvider(url))

# getting the compiling data
contractFile='./contract/compileFile.json'
with open(contractFile) as file:
    compile_data=json.load(file)

#Creating contract
abi=compile_data['contracts']['contractFile.sol']['Bank']['abi']
bytecode=compile_data['contracts']['contractFile.sol']['Bank']['evm']['bytecode']['object']

contract=web3.eth.contract(abi=abi,bytecode=bytecode)

#Deploying contract
account='0xbc94E107B156c7927367B11E57763919826eEE0a'
key='9363ebff72db75f95dc03e3788198b62ce57edb5c1f7e9691d97b6fdb5dd5b5b'

transaction=contract.constructor().buildTransaction(
    {
        'from':account,
        'gasPrice':web3.eth.gas_price,
        'nonce':web3.eth.getTransactionCount(account),
        'chainId':1337,
        'gas':2100000
    }
)

#Signing the transaction
signtx=web3.eth.account.sign_transaction(transaction,key)

#Generating hash
hash=web3.eth.sendRawTransaction(signtx.rawTransaction)

#Generating receipt
receipt=web3.eth.wait_for_transaction_receipt(hash)

#Saving contractAddress
filePath='./contract/contractAddress.txt'
with open(filePath,'w') as file:
    file.write(receipt['contractAddress'])

print('Successfully completed')
