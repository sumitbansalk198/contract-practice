import json
from solcx import compile_standard  #install_solc

filePath='./contract/contractFile.sol'

#For opening the file in read mode
contract=open(filePath)

#For reading the data of the file
contract_data=contract.read()

#closing the file
contract.close()

# compiling the file, we use install_solc('version name') before compiling which we imported from solcx just to have the proper version

# install_solc('0.8.13')
compile_data=compile_standard(

    {
        'language':'Solidity',
        'sources': {
            'contractFile.sol': {
                'content': contract_data
            }
        },
        'settings':{
            'outputSelection':{
                '*':{
                    '*': ['abi','metadata','evm.bytecode','evm.sourceMap']
                }
            }
        }
    }, solc_version ='0.8.13'
)

# For creating a compile data file
compileFilePath='./contract/compileFile.json'
with open(compileFilePath,'w') as file:
    json.dump(compile_data,file)

print('Successfully done')