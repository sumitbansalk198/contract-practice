import json
from web3 import Web3
from flask import Flask,jsonify

#Establishing the connection
url='HTTP://127.0.0.1:7545'
web3=Web3(Web3.HTTPProvider(url))

#For loading contract address
addressFile='./contract/contractAddress.txt'
with open(addressFile) as file:
    contractAddress=file.read()

#For loading abi
compileFile='./contract/compileFile.json'
with open(compileFile) as file:
    compile_data=json.load(file)
abi=compile_data['contracts']['contractFile.sol']['Bank']['abi']

#Creating a instance of contract
contract=web3.eth.contract(address=contractAddress,abi=abi)

#For Transaction
account='0xa86cc25A04224Ba6317a188bFb24e33A03A826a2'
key='0x'+'5ad309e8aca8e346bd0ea56139f757296e68d6be1ce5835b62067b4af48fba25'

def transaction_fn(transaction,key):
    signtx=web3.eth.account.sign_transaction(transaction,key)
    hash=web3.eth.send_raw_transaction(signtx.rawTransaction)
    receipt=web3.eth.wait_for_transaction_receipt(hash)
    return receipt

#Starting Flask
app=Flask(__name__)

#Creating Api
@app.route('/getBalance')
def getBalance():
    print(web3.eth.getTransactionCount(account))
    return jsonify(contract.functions.getBalance().call())
    

@app.route('/withDrow/<int:n>')
def withdraw(n):
    transaction1=contract.functions.withDrow(n).buildTransaction(
        {
            'from':account,
            'chainId':1337,
            'gasPrice':web3.eth.gas_price,
            'nonce':web3.eth.getTransactionCount(account)
        }
    )
    Transactionreceipt=transaction_fn(transaction1,key)
    return str(dict(Transactionreceipt))

@app.route('/deposite/<int:n>')
def deposit(n):
    transaction2=contract.functions.deposite(n).buildTransaction(
        {
            'from':account,
            'chainId':1337,
            'gasPrice':web3.eth.gas_price,
            'nonce':web3.eth.getTransactionCount(account)
        }
    )
    Transactionreceipt=transaction_fn(transaction2,key)
    return str(dict(Transactionreceipt))

# Running Flask
if __name__=='__main__':
    app.run(debug=True)