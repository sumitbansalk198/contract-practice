pragma solidity 0.8.13;

contract Bank {
    
    int bal;
    
    constructor () public
    {
        bal = 1;
    }
    
    function getBalance() view public returns (int)
    {
        return bal;
    }
    function withDrow (int amt) public 
    {
        bal =bal - amt;
    }
    function deposite(int amt) public 
    {
        bal = bal + amt;
    }
}